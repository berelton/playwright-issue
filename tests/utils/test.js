const base = require("@playwright/test");
const { chromium } = require("playwright");
const path = require("path");

exports.test = base.test.extend({
  context: async ({}, use) => {
    const pathToExtension = path.join(__dirname, "keplr");
    const context = await chromium.launchPersistentContext("", {
      recordVideo: {
        dir: path.join(__dirname, "../videos"),
      },
      headless: false,
      args: [
        `--disable-extensions-except=${pathToExtension}`,
        `--load-extension=${pathToExtension}`,
        `--disable-gpu`,
      ],
    });
    await use(context);
    await context.close();
  },
  extensionId: async ({ context }, use) => {
    let [background] = context.backgroundPages();
    if (!background) background = await context.waitForEvent("backgroundpage");

    const extensionId = background.url().split("/")[2];
    await use(extensionId);
  },
});
