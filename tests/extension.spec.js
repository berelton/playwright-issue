const { test } = require("./utils/test");

process.env.DEBUG = "pw:browser*";

function resolveAfter2Seconds() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve("resolved");
    }, 2000);
  });
}

test("popup page", async ({ page, extensionId }) => {
  await page.goto(`chrome-extension://${extensionId}/popup.html`);
  await page.locator("text=Import existing account");
  const result = await resolveAfter2Seconds();
});
